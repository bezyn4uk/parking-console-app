﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Timers;


namespace ParkingSimulator.BL.Models
{
    public class Logger
    {


        private string logFilePath { get; set; }

        public Logger()
        {
            logFilePath = Path.Combine(Environment.CurrentDirectory, "Transactions.log");
        }

        public void WriteLog(IEnumerable<Transaction> transactions)
        {
            using (var streamWriter = new StreamWriter(logFilePath, true, Encoding.Default))
            {
                foreach (var transaction in transactions)
                {
                    streamWriter.WriteLine(transaction);
                }

            }

        }

        public string ReadLog()
        {
            string resultString;
            using (var streamReader = new StreamReader(logFilePath, Encoding.Default))
            {
                resultString = streamReader.ReadToEnd();
            }

            return resultString;
        }


    }
}