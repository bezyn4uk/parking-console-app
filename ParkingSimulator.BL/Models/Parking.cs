﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Timers;

namespace ParkingSimulator.BL.Models
{
    public class Parking
    {
        private static Parking parking = new Parking();

        public double Balance { get; set; }

        public int MaxCapacity { get; set; }

        public int PeriodInSeconds { get; set; }

        public double FineCoefficient { get; set; }

        private List<Transaction> Transactions { get; set; } = new List<Transaction>();
        public Dictionary<VehicleType, double> Rates { get; set; } = new Dictionary<VehicleType, double>();
        public Logger Logger { get; private set; }
        private List<Vehicle> Vehicles { get; set; } = new List<Vehicle>();

        private Timer TransactionsLoggerTimer { get; set; }
        private Timer TransactionTimer { get; set; }
        

        private Parking()
        {
            Console.WriteLine("Parking initilized");
        }

        public static Parking Init()
        {
            parking.Balance = SimulatorSettings.Balance;
            parking.MaxCapacity = SimulatorSettings.MaxParkingCapacity;
            parking.PeriodInSeconds = SimulatorSettings.PeriodInSeconds;
            parking.FineCoefficient = SimulatorSettings.FineCoefficient;
            parking.Rates = SimulatorSettings.Rates;

            parking.Logger = new Logger();

            parking.TransactionsLoggerTimer = new Timer(60000);
            parking.TransactionsLoggerTimer.Elapsed += parking.TransactionLoggerTimerOnElapsed;
            parking.TransactionsLoggerTimer.AutoReset = true;
            parking.TransactionsLoggerTimer.Enabled = true;

            parking.TransactionTimer = new Timer(parking.PeriodInSeconds * 1000);
            parking.TransactionTimer.Elapsed += parking.TransactionTimerOnElapsed;
            parking.TransactionTimer.AutoReset = true;
            parking.TransactionTimer.Enabled = true;

            return parking;
        }

        private void TransactionTimerOnElapsed(object sender, ElapsedEventArgs elapsedEventArgs)
        {
            parking.AddTransactions();
            //Console.WriteLine("Transactions added");
        }

        private void TransactionLoggerTimerOnElapsed(object sender, ElapsedEventArgs elapsedEventArgs)
        {
            parking.Logger.WriteLog(parking.Transactions);
            parking.Transactions = new List<Transaction>();
            //Console.WriteLine("Transactions added to file");
        }

        public void AddVehicle(Vehicle vehicle)
        {
            if (parking.Vehicles.Count <= parking.MaxCapacity)
            {
                parking.Vehicles.Add(vehicle);
            }
            else
            {
                Console.WriteLine($"На стоянке нет свободных мест.");
            }
        }

        public bool RemoveVehicle(int vehicleId)
        {
            var vehicleToRemove = GetVehicleById(vehicleId);

            if (vehicleToRemove == null)
                return false;

            return parking.Vehicles.Remove(vehicleToRemove);
        }

        public Vehicle GetVehicleById(int id)
        {
            return parking.Vehicles.Single(v => v.Id == id);
        }

        public int GetCurrentCapacity()
        {
            return parking.Vehicles.Count;
        }

        public double GetEarnedMoney()
        {
            return parking.Transactions.Sum(v => v.Payment);
        }

        public IEnumerable<Transaction> GetTransactions()
        {
            return parking.Transactions;
        }

        public void AddTransactions()
        {
            foreach (var vehicle in parking.Vehicles)
            {
                var payment = parking.GetPayment(vehicle);
                parking.Transactions.Add(new Transaction()
                {
                    VehicleId = vehicle.Id,
                    Time = DateTime.Now,
                    Payment = payment
                });

                parking.Balance += payment;
            }
        }

        public void GetAllTransactions()
        {
            parking.Logger.ReadLog();
        }

        public void TopUpVehicleBalance(Vehicle vehicle, double amountOfMoney)
        {
            var searchedVehicle = parking.Vehicles.Find(v => Equals(v, vehicle));
            if (searchedVehicle != null)
            {
                searchedVehicle.Balance += amountOfMoney;
            }
        }

        private double GetPayment(Vehicle vehicle)
        {
            if (parking.Rates.TryGetValue(vehicle.Type, out double moneyPerPeriod))
            {
                if (vehicle.Balance < moneyPerPeriod)
                {
                    vehicle.Fine += moneyPerPeriod * FineCoefficient;
                }
                vehicle.Balance -= moneyPerPeriod;
            }
            return moneyPerPeriod;
        }

        public IEnumerable<Vehicle> GetVehicles()
        {
            return parking.Vehicles;
        }
    }
}