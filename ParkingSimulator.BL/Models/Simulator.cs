﻿using System;
using System.Collections.Generic;

namespace ParkingSimulator.BL.Models
{
    public class Simulator
    {
        public Parking Parking { get; set; }
        public Logger logger;



        public Simulator()
        {
            Console.WriteLine("Constructor simulator");
            logger = new Logger();
            Parking = Parking.Init();

        }

        public void Start()
        {
            Console.WriteLine("Simulator starts...");
            Console.WriteLine($"Time: {DateTime.Now}");

            Console.WriteLine($"Parking initialized with settings: ");
            PrintParkingSettings();
            GetUserAction();



        }

        private void PrintParkingSettings()
        {
            Console.WriteLine($"Parking balance: {Parking.Balance}");
            Console.WriteLine($"Parking max capacity: {Parking.MaxCapacity}");
            Console.WriteLine($"Parking period for which funds will be withdrawn : {Parking.PeriodInSeconds} sec");
            PrintParkingRates();
            Console.WriteLine($"Parking fine coefficient: {Parking.FineCoefficient}");
        }

        private void PrintParkingRates()
        {
            Console.WriteLine($"Parking rates: ");
            foreach (var rate in Parking.Rates)
            {
                Console.WriteLine($"Vehicle type:{rate.Key} - {rate.Value}");
            }
        }

        private void GetUserAction()
        {

            var flag = true;
            ConsoleKey userbutton;

            while (flag)
            {
                //Console.Clear();
                Console.WriteLine(new string('=', 50));
                Console.WriteLine($"Functional buttons: ");
                Console.WriteLine($"1: Get parking balance;");
                Console.WriteLine($"2: Get the amount of money earned in the last minute;");
                Console.WriteLine($"3: Get parking amount of free places;");
                Console.WriteLine($"4: Get parking transactions for last minute;");
                Console.WriteLine($"5: Get all parking tarasactions;");
                Console.WriteLine($"6: Get parked vehicles;");
                Console.WriteLine($"7: Park vehicle;");
                Console.WriteLine($"8: Delete vehicle from parking;");
                Console.WriteLine($"9: Top up parking vehicle;");
                Console.WriteLine($"Esc: Exit;");
                Console.WriteLine(new string('=', 50));

                Console.WriteLine("Enter your key: ");
                userbutton = Console.ReadKey().Key;
                switch (userbutton)
                {
                    case ConsoleKey.D1:
                        PrintParkingBalance();
                        break;
                    case ConsoleKey.D2:
                        PrintEarnedMoney();
                        break;
                    case ConsoleKey.D3:
                        PrintAmountFreePlaces();
                        break;
                    case ConsoleKey.D4:
                        PrintLastTransactions();
                        break;
                    case ConsoleKey.D5:
                        PrintAllTransactions();
                        break;
                    case ConsoleKey.D6:
                        PrintParkedVehicles();
                        break;
                    case ConsoleKey.D7:
                        AddVehicle();
                        break;
                    case ConsoleKey.D8:
                        DeleteVehicle();
                        break;
                    case ConsoleKey.D9:
                        TopUpVehicleBalance();
                        break;
                    case ConsoleKey.Escape:
                        flag = false;
                        break;
                    default:
                        Console.WriteLine($"Key undefined");
                        break;

                }
            }
        }

        private void TopUpVehicleBalance()
        {
            Console.WriteLine($"Enter vehicle Id for top up balance: ");
            int userVehicleId;

            while (true)
            {
                bool parsedId = int.TryParse(Console.ReadLine(), out userVehicleId);
                if (!parsedId)
                {
                    Console.WriteLine($"Enter correct number;");
                    continue;
                }

                var vehicle = Parking.GetVehicleById(userVehicleId);

                if(vehicle == null)
                {
                    Console.WriteLine($"Cant find your car, enter valid number;");
                    continue;
                }

                Console.WriteLine($"Enter amount of money to top up balance: ");

                double userMoney;

                while(true)
                {
                    bool parsedDouble = double.TryParse(Console.ReadLine(), out userMoney);
                    if(!parsedDouble)
                    {
                        Console.WriteLine("Enter valid number");
                        continue;
                    }
                    break;
                }

                vehicle.Balance += userMoney;
                Console.WriteLine($"{vehicle} new balance : {vehicle.Balance}");
                break;

            }
        }

        private void DeleteVehicle()
        {
            Console.WriteLine($"Enter vehicle Id for deleting: ");
            int userVehicleId;

            while(true)
            {
                bool parsedId = int.TryParse(Console.ReadLine(), out userVehicleId);
                if(!parsedId)
                {
                    Console.WriteLine($"Enter correct number;");
                }

                if (Parking.RemoveVehicle(userVehicleId))
                {
                    Console.WriteLine($"Vehicle deleted successfully;");
                    break;
                }
            }
        }

        private void AddVehicle()
        {
            Console.WriteLine($"Adding new vehicle...");
            Vehicle vehicle = VehicleInitializing();

            Console.WriteLine($"Parking vehicle {vehicle}: y/n");
            while (true)
            {
                if (Console.ReadKey().Key == ConsoleKey.Y)
                {
                    Parking.AddVehicle(vehicle);
                    break;
                }

                if (Console.ReadKey().Key == ConsoleKey.N)
                {
                    break;
                }
            }

        }

        private Vehicle VehicleInitializing()
        {
            Console.WriteLine($"Enter vehicle model: ");
            string userVehicleModel = Console.ReadLine();

            Console.WriteLine($"Enter vehicle manufacturer: ");
            string userVehicleManufacturer = Console.ReadLine();

            double userVehicleBalance;

            while (true)
            {
                Console.WriteLine($"Enter vehicle balance: ");
                bool doubleParsed = double.TryParse(Console.ReadLine(), out userVehicleBalance);
                if (doubleParsed)
                {
                    break;
                }
            }

            Console.WriteLine($"Choose vehicle type: ");
            PrintVehicleTypes();

            VehicleType userVehicleType;

            while (true)
            {

                bool typeParsed = Enum.TryParse<VehicleType>(Console.ReadLine(), out userVehicleType);
                if (typeParsed)
                {
                    break;
                }
            }

            var vehicle = new Vehicle(userVehicleModel, userVehicleManufacturer, userVehicleType, userVehicleBalance);
            return vehicle;
        }

        private void PrintVehicleTypes()
        {
            int i = 0;
            foreach(var type in Enum.GetNames(typeof(VehicleType)))
            {
                Console.WriteLine($"{i++}: {type}");
            }
        }



        private void PrintParkedVehicles()
        {
            Console.WriteLine($"Parked vehicles: ");
            foreach (var vehicle in Parking.GetVehicles())
            {
                Console.WriteLine(vehicle);
            }
        }

        private void PrintAllTransactions()
        {
            Console.WriteLine($"Parking transactions: ");
            Parking.GetAllTransactions();
        }

        private void PrintLastTransactions()
        {
            Console.WriteLine($"Parking transactions for las minute: ");
            foreach(var transaction in Parking.GetTransactions())
            {
                Console.WriteLine($"{transaction}");
            }
            
        }

        private void PrintAmountFreePlaces()
        {
            Console.WriteLine($"Parking has {Parking.MaxCapacity - Parking.GetCurrentCapacity()} places free.");
        }

        private void PrintEarnedMoney()
        {
            Console.WriteLine($"Parking for las minute earned: {Parking.GetEarnedMoney()}");
        }

        private void PrintParkingBalance()
        {
            Console.CursorLeft=0;
            Console.WriteLine($"Parking balance for {DateTime.Now} : {Parking.Balance}");
        }
    }
}
