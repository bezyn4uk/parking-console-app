﻿using System;
using System.Collections.Generic;

namespace ParkingSimulator.BL.Models
{
    public static class SimulatorSettings
    {
        private static double balance;
        private static int maxParkingCapacity = 10;
        private static int periodInSeconds = 5;

        public static double Balance
        {
            get => balance;
            set
            {
                if (value > 0)
                {
                    balance = value;
                }

                throw new ArgumentException();
            }
        }

        public static int MaxParkingCapacity
        {
            get => maxParkingCapacity;
            set
            {
                if (value > 0)
                {
                    maxParkingCapacity = value;
                }

                throw new ArgumentException();
            }
        }

        public static int PeriodInSeconds
        {
            get => periodInSeconds;
            set
            {
                if (value > 0)
                {
                    maxParkingCapacity = value;
                }

                throw new ArgumentException();
            }
        }

        public static double FineCoefficient { get; set; } = 2.5;

        public static Dictionary<VehicleType, double> Rates { get; set; } = new Dictionary<VehicleType, double>()
        {
            {VehicleType.Sedan, 2},
            {VehicleType.Truck, 5},
            {VehicleType.Bus, 3.5},
            {VehicleType.Motorcycle, 1}
        };

    }
}