﻿using System;

namespace ParkingSimulator.BL.Models
{
    public class Transaction
    {
        public int VehicleId { get; set; }
        public double Payment { get; set; } 
        public DateTime Time { get; set; }

        public override string ToString()
        {
            return $"{VehicleId}\t{Time}\t{Payment};";
        }
    }
}