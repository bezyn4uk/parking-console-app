﻿using System;
using System.Timers;


namespace ParkingSimulator.BL.Models
{
    public class Vehicle: IEquatable<Vehicle>
    {
        private static int count = 0;
        public int Id { get; private set; }

        public string Model { get; private set; }

        public string Manufacturer { get; private set; }

        public double Balance { get; set; }
        public double Fine { get; set; }

        public VehicleType Type { get; private set; }

        public Vehicle(string model, string manufacturer, VehicleType vehicleType, double balance) 
        {
            count += 1;
            Id = count;
            Model = model;
            Manufacturer = manufacturer;
            Type = vehicleType;
            Balance = balance;
        }

        public override string ToString()
        {
            return $"{Id}: {Model}, {Manufacturer}, {Balance}, {Fine}";
        }


        public bool Equals(Vehicle other)
        {
            if (ReferenceEquals(null, other)) return false;
            if (ReferenceEquals(this, other)) return true;
            return Id == other.Id && string.Equals(Model, other.Model) && string.Equals(Manufacturer, other.Manufacturer) && Type == other.Type;
        }

        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj)) return false;
            if (ReferenceEquals(this, obj)) return true;
            return obj.GetType() == this.GetType() && Equals((Vehicle) obj);
        }

        public override int GetHashCode()
        {
            unchecked
            {
                var hashCode = Id;
                hashCode = (hashCode * 397) ^ (Model != null ? Model.GetHashCode() : 0);
                hashCode = (hashCode * 397) ^ (Manufacturer != null ? Manufacturer.GetHashCode() : 0);
                hashCode = (hashCode * 397) ^ (int) Type;
                return hashCode;
            }
        }
    }
}