﻿namespace ParkingSimulator.BL.Models
{
    public enum VehicleType
    {
        Bus,
        Sedan, 
        Truck,
        Motorcycle
    }
}