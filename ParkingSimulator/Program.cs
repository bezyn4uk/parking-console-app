﻿using ParkingSimulator.BL.Models;
using System;

namespace ParkingSimulator
{
    class Program
    {
        static void Main(string[] args)
        {
            var newGame = new Simulator();
            newGame.Start();
        }
    }
}
